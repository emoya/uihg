# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require 'uihelper/rails/version'

Gem::Specification.new do |s|
  s.name        = "uihelper-rails"
  s.version     = Uihelper::Rails::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Sears"]
  s.email       = ["kcd_dev@searshc.com"]
  s.summary     = "UI Helpers, SASS to be reused across Rails projects"
  s.description = "UI Helpers, SASS to be reused across Rails projects and autoincluded Rails 3.1+ asset pipeline"

  s.required_rubygems_version = ">= 1.3.6"

  s.add_dependency "railties", ">= 3.1.0"

  s.files         = Dir["./lib/*.rb", "./lib/**/*.rb", "./vendor/**/*"]
  s.require_paths = ['lib']
end
