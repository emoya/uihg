require File.join(File.expand_path("../../../uihelper/generators", __FILE__), 'responsive.rb')

class ResponsiveGenerator < Rails::Generators::Base
  source_root File.expand_path("../templates", __FILE__)
  argument :layout_name, :type => :string, :default => "layout"
  argument :breaks, :type => :hash, :default => {"desktop" => 960, "tablet" => 768, "mobile" => 480}

  def build
    Generators::Responsive.new(layout_name, breaks).build
  end
  
end
