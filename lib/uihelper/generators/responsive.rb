require 'erb'
require File.join(File.expand_path("../../../uihelper/rails", __FILE__), 'version.rb')

module Generators
  class Responsive
    def initialize(layout_name, breaks, base_directory="app/assets/stylesheets", has_asset_pipeline=true)
      breaks.each {|k,v|  breaks[k] = v.to_i}
      @layout_name = layout_name
      @breaks = breaks
      @base_directory = base_directory
      @has_asset_pipeline = has_asset_pipeline
    end

    def build
      ensure_base_layout
      break_model = read_break_model
      unless break_model
        break_model = create_new_break_model
        append_break_model(break_model, application_css_scss)
        append_break_model(break_model, ie_css_scss)
      end
      insert_break_points(break_model)
      add_break_scss_files(break_model)
      unless File.exists?(_config_css_scss)
        copy_config_css_scss_template_to(break_model, _config_css_scss)
      end
    end

    def ensure_base_layout
      FileUtils.mkdir_p @base_directory
      unless File.exists?(ie_css_scss)
        FileUtils.cp(ie_css_scss_template, ie_css_scss)
      end
      unless File.exists?(application_css_scss)
        copy_application_css_scss_template_to(application_css_scss)
      end
    end

    private 

    def add_break_scss_files(break_model)
      break_model.keys.each do |breakpoint| 
        break_dir = File.join(@base_directory, breakpoint.to_s)
        break_filename = with_scss_extension("_#{@layout_name}")
        FileUtils.mkdir_p break_dir
        if ! File.exists? File.join(break_dir, break_filename)
          File.open(File.join(break_dir, break_filename), 'w') do |fhandle|
            fhandle.puts(generate_breakpoint_mixin_directive(breakpoint))
          end
        end
      end
    end

    ["ie", "application", "_config"].each do |basename|
      define_method ("#{basename}_css_scss") do
        File.join(@base_directory, with_scss_extension(basename))
      end

      define_method ("#{basename}_css_scss_template") do
        File.join(File.dirname(__FILE__), "#{basename}_css_scss.txt")
      end
    end

    def copy_application_css_scss_template_to(destination)
      asset_require_directive = (@has_asset_pipeline ? "/*\n *= require_self\n*/" : "")
      vers = Uihelper::Rails::VERSION
      str = ERB.new(File.read(File.join(File.dirname(__FILE__), "application_css_scss.erb"))).result(binding)
      File.open(destination, 'w') { |fhandle| fhandle.write(str)}
    end

    def with_scss_extension(basename)
      basename + (@has_asset_pipeline ? ".css.scss" : ".scss" )
    end

    def create_new_break_model
      break_model = @breaks.inject({}) { |res, (k,v)| res[v] = {:breakname => k}; res }
      previous_max = nil
      previous_break = nil
      break_model.keys.sort.each do |size|
        if previous_break
          unless break_model[previous_break][:maxwidth]
            previous_max = break_model[previous_break][:maxwidth] = ((previous_break + size) / 2).round
          end
          break_model[size][:minwidth] = previous_max + 1
        else
          previous_max = break_model[size][:maxwidth] = size
        end
        previous_break = size
      end
      break_model
    end


    def append_break_model(break_model, file)
      
      File.open(file, 'a') do |fhandle|
        fhandle.puts generate_breakmodel_header(break_model)
        break_model.keys.sort.each do |break_size|
          fhandle.puts "@import \"#{break_size}/*\";"
        end

        fhandle.puts ""
        fhandle.puts ""
        break_model.keys.sort.each do |break_size|
          fhandle.puts generate_breakpoint_header(break_size)
          fhandle.write("@media only screen")
          fhandle.write(" and (min-width: #{break_model[break_size][:minwidth]}px)") if break_model[break_size][:minwidth]
          fhandle.write(" and (max-width: #{break_model[break_size][:maxwidth]}px)") if break_model[break_size][:maxwidth]
          fhandle.puts(' {')
          fhandle.puts('}')
        end
      end
      break_model
    end

    def insert_break_points(break_model)
      break_model.keys.sort.each do |breakpoint|
        unless has_breakpoint(application_css_scss, breakpoint, @layout_name)
          bp_end = find_breakpoint_end(breakpoint)
          bp_command = "  @include #{breakpoint_name(breakpoint)};"
          insert_before_line(application_css_scss, bp_command, bp_end)
        end
      end
    end

    def generate_breakpoint_mixin_directive(breakpoint)
      "@mixin #{breakpoint_name(breakpoint)} {\n  //Place #{breakpoint}px styles here\n  \n}"
    end

    def has_breakpoint(file, breakpoint, layout_name)
      start_line = find_breakpoint_start(breakpoint)
      end_line = find_breakpoint_end(breakpoint)
      line_count = 1
      File.open(file).each_line do |line|
        if (start_line < line_count) && (line_count < end_line)
          return true if line =~ / breakpoint_name(breakpoint, layout_name)/
        end
        line_count += 1
      end
      return false
    end

    def breakpoint_name(breakpoint, layout_name=@layout_name)
      "#{layout_name}-#{breakpoint}"
    end

    def generate_breakmodel_header(break_model)
      str = @breaks.inject("") {|str, (k,v)| str + "#{v},#{k} "}.chop
      "// Break Model: #{str}"
    end

    def read_break_model
      File.open(application_css_scss).each_line do |line|
        m = /^\/\/ Break Model: (.+)/.match line
        if m
          retVal = {}
          m[1].split(" ").each do |kv|
            (k,v) = kv.split(",")
            retVal[k.to_i] = {:breakname => v}
          end
          return retVal
        end
      end
      nil
    end
    
    def generate_breakpoint_header(break_size)
      "// Break Point: #{break_size}"
    end

    def find_breakpoint_start(break_size)
      breakpoint_found = false
      line_count = 1
      File.open(application_css_scss).each_line do |line|
        m = /^\/\/ Break Point: (.+)/.match line
        (return line_count) if m && m[1].to_i == break_size
        line_count += 1
      end
      raise "Break point start for #{break_size} not found"
    end

    def find_breakpoint_end(breakpoint)
      breakpoint_start = find_breakpoint_start(breakpoint)
      line_count = 1
      File.open(application_css_scss).each_line do |line|
        if line_count > breakpoint_start
          return line_count if line.strip == '}'
        end
        line_count += 1
      end
      raise "Break point end for #{break_size} not found"
    end

    def insert_after_line(file, str, line_num)
      all_lines = File.open(file).readlines
      File.open(file, 'w') do |fhandle|
        line_count = 1
        all_lines.each do |line|
          fhandle.write(line)
          if line_count == line_num
            fhandle.puts(str)
          end
          line_count += 1
        end
      end
    end
    
    def insert_before_line(file, str, line_num)
      all_lines = File.open(file).readlines
      File.open(file, 'w') do |fhandle|
        line_count = 1
        all_lines.each do |line|
          if line_count == line_num
            fhandle.puts(str)
          end
          fhandle.write(line)
          line_count += 1
        end
      end
    end

    def copy_config_css_scss_template_to(break_model, destination)
      model = break_model.inject({}) do |h, (breakpoint, details)|
        h[breakpoint] = details[:breakname]
        h
      end
      str = ERB.new(File.read(File.join(File.dirname(__FILE__), "config_css_scss.erb"))).result(binding)
      File.open(destination, 'w') { |fhandle| fhandle.write(str)}      
    end

  end
end
