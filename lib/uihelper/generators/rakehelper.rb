module Generators
  class Rakehelper
    def self.args_to_hash(*args)
      args.inject({}) do |h, arg| 
        if arg.is_a? String
          (name,br) = arg.split(':')
          raise "Invalid breakpoint: #{br}; must be integer" if br.nil? or br.to_i == 0
          h[name] = br
        end
        h
      end
    end
  end
end
