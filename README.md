# UIHelper Documentation
The UIHelper Gem is composed of a Sass library of functions and mixins and a generator intended to help kickstart a Responsive site. The library's functions are useful for a Responsive site, but can be used for non-Responsive ones as well. They are:

* [UIHelper Mixins](#mixins)
  * [col_layout](#col_layout)
  * [rowless-grid](#rowless-grid)
* [UIHelper Functions](#functions)
  * [Math](#math)
  * [Utilities](#utilities)
* [Generator](#generator)
  * [File Schema](#files)
  * [Example](#example)
  * [Output](#ouput)

<h2 id="mixins">UIHelper Mixins</h2>
1. ### `col-layout( $column-widths`, _`$margin`_, _`$padding`_, _`$total-width`_, _`$float`_, _`$strategy`_ `)`
    **`col-layout`**:<a id="col_layout"></a>
    A mixin that automatically creates the necessary css for a columnar layout of a set of elements. It accepts 6 parameters but requires only 1, **`$column-widths`**.
  
    **`$column-widths`** _Required_
    A list of column widths in pixels, listed in the order from left to right as you want them to appear in the HTML.
    **Example**
    (200, 450, 250)
  
    **`$margin`** _Optional_
    A number in pixels representing the size of the margin between the columns. By default, this gets applied as a left margin on any column after the first. If the **`$float`** parameter is set to "right", the margin will be applied to the right.
  
    **`$padding`** _Optional_
    A number or a list in pixels representing the size of the padding applied to the columns. If a single value is supplied, this padding will be applied to all columns. If a list is supplied, each value will be applied to the corresponding column in **`$column-widths`**. When padding is present, its value is subtracted from columns that have it applied. For example, 
  
        col-layout( (200, 450, 250), 0, 15 )
  
    will recalculate the column widths as
      
        (170, 420, 220)
  
    and
  
        col-layout( (200, 450, 250), 0, (0, 15, 5) )
  
    as
  
        (200, 420, 240)
  
    **`$total-width`** _Optional_
    A percentage that all the widths, **`$column-widths`**, **`$margin`** and **`$padding`** should total up to. 100% is the default, but any percentage is allowed.
  
    **`$float`** _Optional_
    Determines which direction the elements will float. Accepts "left" or "right"; "left" is default.
  
    **`$strategy`** _Optional_
    Determines which version of css selectors to be used. Accepts "css2" or "css3"; "css2" is default.
  
    #### Full Usage Example
  
        #columns {
          @include col-layout( (585, 190), 24, (0, 16) );
        }
  
    _outputs_
  
        #columns > * {
          width: 73.216%;
          float: left;
          padding: 0%;
        }
        #columns > * + * {
          width: 19.777%;
          float: left;
          padding: 2.002%;
          margin-left: 3.003%;
        }

2. ### `rowless-grid( $cell-selector`, `$grid-columns`, `$total-width`, _`$margin`_, _`$padding`_, _`$max-cells`_, _`$strategy`_ )
    **`rowless-grid`**:<a id="rowless-grid"></a>  
    A mixin that automatically creates the necessary css for a gridded layout of a set of elements. It accepts 7 parameters and requires 3, **`$cell-selector`**, **`$grid-columns`** and **`$total-width`**.
  
    **`$cell-selector`** _Required_
    The name of the class or HTML element to have the grid applied to.
  
    **`$grid-columns`** _Required_
    The number of columns the grid is to have.
  
    **`$total-width`** _Required_
    A value that all the widths, **`$column-widths`**, **`$margin`** and **`$padding`** should total up to. 100 is recommended, but any integer is allowed.
  
    **`$margin`** _Optional_
    A number in pixels representing the size of the margin between the columns. This will get applied as a left margin on any column after the first and as top margin on any row after the first.
  
    **`$max-cells`** _Optional_
    Used only when "css2" is passed into **`$strategy`**. The number of times the CSS2 selectors should be made into the generated css. Default is 20.
  
    **`$strategy`** _Optional_
    Determines which version of css selectors to be used. Accepts "css2" or "css3"; "css3" is default. The reason "css3" is the default option here is because it generates much less css. See below for examples.
  
    #### Full Usage Examples
        #landing {
          @include rowless-grid(".landingbox", 4, 100, 2);
        }
  
     _outputs_
  
        #landing .landingbox:nth-child(4n+0) {
            float: left;
            width: 23.584%;
            margin-left: 1.886%;
          }
          #landing .landingbox:nth-child(4n+1) {
            float: left;
            width: 23.584%;
            clear: both;
          }
          #landing .landingbox:nth-child(4n+2) {
            float: left;
            width: 23.584%;
            margin-left: 1.886%;
          }
          #landing .landingbox:nth-child(4n+3) {
            float: left;
            width: 23.59%;
            margin-left: 1.886%;
          }
          #landing .landingbox:not(:nth-child(-n+4)) {
            margin-top: 1.886%;
          }
  
    By contrast,
  
        #landing {
          @include rowless-grid(".landingbox", 4, 100, 2, $strategy: "css2");
        }
  
    _outputs_
  
        #landing .landingbox {
          margin-left: 1.886%;
          float: left;
          width: 23.584%;
        }
        #landing .landingbox + .landingbox + .landingbox + .landingbox + .landingbox {
          margin-top: 1.886%;
        }
        #landing .landingbox:first-child {
          margin-left: 0;
          clear: both;
          width: 23.59%;
        }
        #landing .landingbox:first-child + .landingbox + .landingbox + .landingbox + .landingbox {
          margin-left: 0;
          clear: both;
          width: 23.59%;
        }
        #landing .landingbox:first-child + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox {
          margin-left: 0;
          clear: both;
          width: 23.59%;
        }
        #landing .landingbox:first-child + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox {
          margin-left: 0;
          clear: both;
          width: 23.59%;
        }
        #landing .landingbox:first-child + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox + .landingbox {
          margin-left: 0;
          clear: both;
          width: 23.59%;
        }

<h2 id="functions">UIHelper Functions</h2>
Included in the UIHelper gem are a number of additional useful functions, most of which are used directly by **`col-layout`** and **`rowless-grid`**, while others are simply generally useful functions.

<h3 id="math">Math</h3>
**`sum( $num-list )`**
Accepts a list of numbers and returns the sum of them.
**Example:** `sum(10, 5, 2) => 17`

**`subtract( $num-list)`**
Accepts a list of numbers and returns the total after subtracting each one in sequence.
**Example:** `subtract(10, 5, 2) => 3`

**`multiply( $num-list )`**
Accepts a list of numbers and returns the total after multiplying each one in sequence.
**Example:** `multiply(10, 5, 2) => 100`

**`divide( $num-list )`**
Accepts a list of numbers and returns the total after dividing each one in sequence.
**Example:** `divide(10, 5, 2) => 1`

**`power( $number, $exponent )`**
Returns the total after multiplying **`$number`** **`$exponent`** times.
**Example:** `power(10, 2) => 100`

**`to-decimal-places( $number,` _`$digits`_ `)`**
Returns **`$number`** to **`$digits`** number of significant digits dropping anything beyond it. **`$digits`** is 2 by default since Sass has a default of returning 3 significant digits.
**Example:** `to-decimal-places(10.129, 2) => 10.12`

**`ratioize($num-list,` _`$total-pct`_, _`$remainder-idx`_ `)`**
Accepts a list of numbers and returns their relative percentages as a list in the same order as the input. To ensure that the total equals exactly **`$total-pct`** (which by default is 100, but can be any integer), an adjustment happens on the member of the list identified in **`$remainder-idx`** to accept a remainder, should there be any. By default, this will be the last member of the list.
**Example 1:** `ratioize((10, 10, 10))  => (33.333%, 33.333%, 33.334%)`
**Example 2:** `ratioize((10, 10, 10), 100%, 2)  => (33.333%, 33.334%, 33.333%)`
**Example 3:** `ratioize((10, 10, 10), 75%)  => (25%, 25%, 25%)`

<h3 id="utilities">Utilities</h3>
**`is_list( $value )`**
Returns true or false whether **`$value`** is a list type.
**Example:** `is_list((10, 5, 2)) => true`

**`is_number( $value )`**
Returns true or false whether **`$value`** is a number type.
**Example 1:** `is_number(5) => true`
**Example 2:** `is_number('5') => false`

**`is_string( $value )`**
Returns true or false whether **`$value`** is a string type.
**Example:** `is_string('Sass') => true`

**`is_bool( $value )`**
Returns true or false whether **`$value`** is a boolean type.
**Example:** `is_bool((10, 5, 2)) => false`

**`is_color( $value )`**
Returns true or false whether **`$value`** is a color type.
**Example:** `is_color(#999) => true`

**`lookup( $lookup-keys, $all-keys, $all-values )`**
Accepts a list of keys or single key, finds those keys in the list, **`$all-keys`**, and returns those members from the list, **`$all-values`**. This is intended as a work-around for Sass' lack of hash tables.
**Example:** `lookup( (c,b), (a,b,c,d), (1,2,3,4) ) => (3,2)`

**`font-size( $target-size, $relative-size )`**
Returns either a percentage or an em based on the desired font-size and the relative font-size. **`relative-size`** has a default font-size of 16, but it can be overwritten either by including a second parameter in the function, or by declaring the variable, **`$default-font-size`**, anywhere in your stylesheet (**`_config.scss`** is recommended).
**Example 1:** `font-size(11) => 68.75%` 
**Example 2:** `font-size(11em) => 0.687em`

Further, overriding the 2nd parameter can be made very useful when nesting font sizes. For example

        .container {
          font-size: font-size(11);
          h1 {
              font-size: font-size(24, 11);
              span {
                  font-size: font-size(16, 24);
              }
          }
        }

_outputs_

        .container {
            font-size: 68.75%;
        }
        .container h1 {
            font-size: 218.181%;
        }
        .container h1 span {
            font-size: 66.666%;
        }

<h2 id="generator">Generator</h2>
The UIHelper Generator is intended to help kickstart a responsive site by creating a folder structure and set of Sass files. Inside a Rails 3 app, this is done as a typical Rails 3 generator and fits into the asset pipeline while, in other environments it works as a rake task.

The file structure it creates is intended to break up the tasks of a stylesheet into the pieces that make sense to the developer, while at the same time imposing standards. The developer's sensibility is reflected in the individual files that get created inside the breakpoint directories. The standards that get imposed are

* consolidating all the css into one file
* consolidating variables into one file
* using the @media formulation for media queries to allow IE8 Javascript solutions to function
* using breakpoints as directories, part of mixins and width arrays.

Of course, like all other generators, the developer is free to change anything after creation.

<h3 id="files">File Schema</h3>
The generator will create 2 files at the root level of its parent directory, **`_config`** and **`application`** _(Note: the file extensions will also be dependent on the environment - for Rails 3, it will be **`.css.scss`**, everywhere else **`.scss`** )_.

**`application`** puts together all the pieces and outputs one consolidated css file, **`application.css`**.

**`_config`** should contain any variable your project will require, but will certainly contain the generated lists based on the breakpoints you create.

Below is an abstracted view of the file structure the generator creates.

    Parent Directory
              |_ breakpoint1
              |       |_ _file1
              |       |_ _file2
              |       |_ _file3
              |_ breakpoint2
              |       |_ _file1
              |       |_ _file2
              |       |_ _file3
              |_ _config
              |_ application

To run the generator, enter into a terminal window

### _`rake`_ `responsive:generate[filename,folderpath,breakpointName:breakpointValue]`

1. **`folderpath`** will house all the generated files. Paths are relative to the gem.

2. **`breakpointName`** will create an function in **`_config`** that can be called as a shortcut to lookup any value in the **`breakpointValue`** list.

3. **`breakpointValue`** will create

    * a directory with **`breakpointValue`** as its name that will house any file that modify classes to have different appearances or behaviors at that size
    * an entry into **`_config`** of a list with **`breakpointValue`** as part of its name containing 3 default values
    * entries into **`application`** that
      * glob the contents of the new directory
      * create the media query at **`breakpointValue`**

4. **`filename`** will create

    * a file, **`_filename`**, inside each breakpoint directory.
    * inside **`_filename`**, a new mixin, the name of which is a combination of itself and its parent directory.
    * entries into **`application`** that
      * insert the call to the newly created mixins inside each media query
      * import the files through an **`@import`** statement.

<h3 id="example">Example</h3>

    rake responsive:generate[layout,../../Sass,mobile:480,tablet:768,desktop:960]

generates

    root
      |_ Site
            |_ assets
                    |_ Sass
                          |_ 480
                          |   |_ _layout.scss
                          |_ 768
                          |   |_ _layout.scss
                          |_ 960
                          |   |_ _layout.scss
                          |_ _config.scss
                          |_ application.scss

**`_config.scss`** will contain:

    // master list of sizes for all layouts
    $width-types:( max, col-l, col-r );
    $widths-480: ( 480, 160, 320 );
    $widths-768: ( 768, 256, 512 );
    $widths-960: ( 960, 320, 640 );

    // functions
    @function mobile($lookups, $types: $width-types, $widths: $widths-480) {
      @return lookup($lookups, $types, $widths);
    }

    @function tablet($lookups, $types: $width-types, $widths: $widths-768) {
      @return lookup($lookups, $types, $widths);
    }

    @function desktop($lookups, $types: $width-types, $widths: $widths-960) {
      @return lookup($lookups, $types, $widths);
    }

**`application.scss`** will contain:

    @import "uihelper";

    @import "config";

    // Break Model: 480,mobile 768,tablet 960,desktop
    @import "480/layout";
    @import "768/layout";
    @import "960/layout";

    // Break Point: 480
    @media only screen and (max-width: 480px) {
      @include layout-480;
    }
    // Break Point: 768
    @media only screen and (min-width: 481px) and (max-width: 864px) {
      @include layout-768;
    }
    // Break Point: 960
    @media only screen and (min-width: 865px) {
      @include layout-960;
    }

**`480/layout.scss`** will contain:

    @mixin layout-480 {
      // 480px layout styles
    
    }

<h3 id="output">Output</h3>

The generated css is created by 2 different means, depending on your environment:

  1. Inside a Rails 3 asset pipeline, the task is handled automatically at runtime in development and with a precompile command in production.
  2. A watch command is started from the terminal that recompiles the assets into a designated output folder on saving, editing or deleting the files inside the watched directory.

The first requires no further discussion, but the second would benefit from a brief example, because it requires the load path of the gem to be included:

    sass --watch path/to/SassInput:path/to/output -I path/to/gem/uihelper-rails_gem/app/assets/stylesheets/